package gitlab.com.models;

import gitlab.com.contracts.Creature;
import gitlab.com.enums.State;

public class BaseAnimals implements Creature {

    private int currentLiveLevel;
    private int currentHungerLevel;
    private int movementSpeed;
    private State state;
    protected int creaturId;
    private static int baseCount = 0;

    public BaseAnimals(int currentLiveLevel, int currentHungerLevel, int movementSpeed) {
        this.currentLiveLevel = currentLiveLevel;
        this.currentHungerLevel = currentHungerLevel;
        this.movementSpeed = movementSpeed;
        this.state = State.LIVE;
        this.creaturId = baseCount;
        baseCount++;
    }

    @Override
    public void eat(Creature creature) {

    }

    @Override
    public int getMoveSpeed() {
        return 0;
    }

    @Override
    public int getCurrentLiveLevel() {
        return this.currentLiveLevel;
    }

    @Override
    public void setCurrentLiveLevel(int l) {
        this.currentLiveLevel = l;
    }

    @Override
    public int getAttack() {
        return 0;
    }

    @Override
    public int getCurrentHungerLevel() {
        return this.currentHungerLevel;
    }

    @Override
    public void setCurrentHungerLevel(int h) {
        this.currentHungerLevel = h;
    }

    @Override
    public void update() {

    }

    @Override
    public State getState() {
        return this.state;
    }
    @Override
    public void setState(State state){
        this.state = state;
    }
}
