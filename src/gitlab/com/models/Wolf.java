package gitlab.com.models;

import gitlab.com.contracts.Carnivorous;
import gitlab.com.contracts.Creature;

public class Wolf extends BaseAnimals implements Carnivorous {
    public Wolf(int currentLiveLevel, int currentHungerLevel, int movementSpeed) {
        super(currentLiveLevel, currentHungerLevel, movementSpeed);
    }

    @Override
    public void kill(Creature livable) {

    }

    @Override
    public String toString() {
        return String.format("Wolf: Current hunger level %d\n\"Wolf: Current live level %d", this.getCurrentHungerLevel(), this.getCurrentLiveLevel());
    }


    @Override
    public void update() {
        if (this.getCurrentHungerLevel() == 0) {
            this.setCurrentLiveLevel(this.getCurrentLiveLevel() - 5);
        } else {
            this.setCurrentHungerLevel(this.getCurrentHungerLevel() - 2);
        }
    }
}
