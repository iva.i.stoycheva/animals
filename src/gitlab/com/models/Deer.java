package gitlab.com.models;

import gitlab.com.contracts.Herbivorous;
import gitlab.com.contracts.Plant;

public class Deer extends BaseAnimals implements Herbivorous {
    private static int hungerRate = 1;
    private static  int liveDegreaseRate = 1;


    public Deer(int currentLiveLevel, int currentHungerLevel, int movementSpeed) {
        super(currentLiveLevel, currentHungerLevel, movementSpeed);
    }

    @Override
    public void graze(Plant plant) {

    }

    @Override
    public String toString() {
        return String.format("Deer: %d, Current HUNGER Level: %d, currentLiveLevel: %d",this.creaturId, this.getCurrentHungerLevel(), this.getCurrentLiveLevel());

    }

    @Override
    public void update() {
        int currentHungerLevel = this.getCurrentHungerLevel() - hungerRate;
        if(currentHungerLevel <= 0){
           int getCurrentLiveLevel = getCurrentLiveLevel() - liveDegreaseRate;
           this.setCurrentLiveLevel(getCurrentLiveLevel);
        }

        this.setCurrentHungerLevel(currentHungerLevel);
    }
}
