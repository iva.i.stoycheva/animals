package gitlab.com.contracts;

public interface Plant {
    void grow();
}
