package gitlab.com.contracts;

import gitlab.com.enums.State;

public interface Simulation {
    void update();
    State getState();
    void setState(State state);
}
