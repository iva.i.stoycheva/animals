package gitlab.com.contracts;

public interface Creature extends Simulation {
    void eat(Creature creature);
    int getMoveSpeed();
    int getCurrentLiveLevel();
    void setCurrentLiveLevel(int l);
    int getAttack();
    int getCurrentHungerLevel();
    void setCurrentHungerLevel(int h);
    String toString();

}
