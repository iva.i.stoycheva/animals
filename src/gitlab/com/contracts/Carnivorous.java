package gitlab.com.contracts;

public interface Carnivorous {
    void kill(Creature livable);
}
