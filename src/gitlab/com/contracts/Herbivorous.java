package gitlab.com.contracts;

public interface Herbivorous {
    void graze(Plant plant);
}
