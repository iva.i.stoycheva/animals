package gitlab.com.engine;


import gitlab.com.contracts.Creature;
import gitlab.com.contracts.Plant;
import gitlab.com.models.Deer;
import gitlab.com.models.Wolf;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Engine {
    private int tick;
    private List<Creature> creatures;
    private List<Plant> plants;

    public Engine() {
        this.creatures = new ArrayList<Creature>();
        this.plants = new ArrayList<Plant>();
    }

    private void tick(){
        System.out.println(String.format("Current tick: %d", this.tick));
        for (Creature c : this.creatures) {
            c.update();
            System.out.println(c.toString());
        }
        for (Plant p : this.plants) {
            System.out.println(p.toString());
        }
        tick++;
    }

    public void run() {
        Wolf wolf = new Wolf(100, 100, 15);
        Deer deer = new Deer(100, 100, 20);
        this.creatures.add(wolf);
        this.creatures.add(deer);

        while (true){
            tick();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
